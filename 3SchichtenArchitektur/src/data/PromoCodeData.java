package data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class PromoCodeData implements IPromoCodeData {

	private List<String> promocode;
	
	public PromoCodeData(){
		this.promocode = new LinkedList<String>();
	}
	
	@Override
	public boolean savePromoCode(String code) {
		try {
			FileWriter fwWrite = new FileWriter("prmocodes.txt", true);
			PrintWriter pwWrite = new PrintWriter (fwWrite);
			pwWrite.println(code);
			pwWrite.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return this.promocode.add(code);
	}

	@Override
	public boolean isPromoCode(String code) {
		return this.promocode.contains(code);
	}

}
